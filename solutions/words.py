def word_split(input):
    return len(set(input.split())) #make it a set to count individual words
