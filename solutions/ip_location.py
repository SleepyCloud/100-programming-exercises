import requests
from bs4 import BeautifulSoup

def ip_location(ip):
    r = requests.get('http://whatismyipaddress.com/ip/' + ip)
    soup = BeautifulSoup(r.text)

    cells = soup.find_all('td')

    return cells[11].text + ", " + cells[10].text + ", " +  cells[9].text
