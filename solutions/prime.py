def is_prime(n):
    if n % 2 == 0:
        return False
    else:
        for i in range(2,int(n+1/2)):
            if n % i == 0:
                return False

    return True

def find_prime(n):
    primes = []
    count = 0
    num = 1

    while count <= n:
       if is_prime(num):
        count+=1
        primes.append(num)
       num+= 1
    return primes
