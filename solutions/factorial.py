def rec_factorial(n):
    if n <= 1:
        return 1
    return n  * factorial(n-1)

def iter_factorial(n):
    total = 1
    numbers = range(2, n+1)

    if n == 0 or n == 1:
        return 1

    for i in numbers:
        total *=  i

    return total