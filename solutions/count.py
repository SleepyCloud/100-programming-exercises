vowels=['a','e','i','o','u']

def vowel_count(input):
    count= 0
    for vowel in vowels:
        count+= input.count(vowel)
    return count

def vowel_indiv_count(input):
    counts = []
    for vowel in vowels:
        counts.append(input.count(vowel))
    return counts

