import socket

def check_open_ports(host, start, end):
    open_ports = []
    for i in range(start, end):
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(1)
            s.connect((host, i))
            open_ports.append(i)
        except socket.timeout:
            pass
    return open_ports

