def fibonacci_iter(limit):
    sequence = []
    a = 1
    b = 1

    while True:
        sequence.append(b)
        a,b = b, a+b
        if b >= limit:
            break
    return sequence
