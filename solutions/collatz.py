def collatz(num):
    count = 0
    if num > 1:#start with num > 1
        while not num == 1:
            if num % 2 == 0: #if num is even divide by 2
                num/=2
            else:#if it's odd multiply by 3 and add 1
                num*=3
                num+=1
            count+=1
    return count

collatz(3)